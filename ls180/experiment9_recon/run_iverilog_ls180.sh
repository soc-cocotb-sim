#!/bin/sh

SRCDIR=../../soclayout/experiments9/non_generated/
cp $SRCDIR/full_core_4_4ksram_litex_ls180_recon.v litex_ls180.v
cp $SRCDIR/full_core_4_4ksram_libresoc_recon.v libresoc.v
cp $SRCDIR/pll.v .
cp $SRCDIR/ls180.v .

touch mem.init mem_1.init mem_2.init mem_3.init mem_4.init
# Only run test in reset state as running CPU takes too much time to simulate
make \
  SIM=icarus \
  TOPLEVEL=ls180 \
  COCOTB_RESULTS_FILE=results_iverilog_ls180.xml \
  COCOTB_HDL_TIMEUNIT=100ps \
  TESTCASE="idcode_reset,idcodesvf_reset,boundary_scan_reset" \
  SIM_BUILD=sim_build_iverilog_ls180


