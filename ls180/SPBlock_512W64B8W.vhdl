-- SPBlock_512W64B8W simulation model
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SPBlock_512W64B8W is
port (
    CLK:    in std_logic;
    A:      in std_logic_vector(8 downto 0);
    D:      in std_logic_vector(63 downto 0);
    Q:      out std_logic_vector(63 downto 0);
    -- Width of WE determines the write granularity
    WE:     in std_logic_vector(7 downto 0)
);
end entity SPBlock_512W64B8W;

architecture rtl of SPBlock_512W64B8W is
    constant WORDS: integer := 512;
    constant WEBITS: integer := WE'length;
    constant WEWORDBITS: integer := 8;
    type word is array (WEBITS - 1 downto 0) of std_logic_vector(WEWORDBITS - 1 downto 0);
    type ram_type is array (0 to WORDS - 1) of word;

    signal RAM:       ram_type;
    signal A_hold:    std_logic_vector(A'range);

    signal addr:      integer;
    signal addr_hold: integer;
    begin
    addr <= to_integer(unsigned(A));
    addr_hold <= to_integer(unsigned(A_hold));

    process(CLK) is
    begin
        if (rising_edge(CLK)) then
        A_hold <= A;
        for weword in 0 to WEBITS - 1 loop
            if WE(weword) = '1' then
                -- Write cycle
                RAM(addr)(weword) <= D((weword + 1)*WEWORDBITS - 1 downto weword*WEWORDBITS);
            end if;
        end loop;
        end if;
    end process;

    read: for weword in 0 to WE'length - 1 generate
    begin
        Q((weword + 1)*WEWORDBITS - 1 downto weword*WEWORDBITS) <= RAM(addr_hold)(weword);
    end generate;
end architecture rtl;
