#!/bin/sh

# create dummy memory files
yes 0 | head -128 > mem_1.init 
yes 0 | head -32 > mem_1.init 
touch mem.init mem_1.init mem_2.init mem_3.init mem_4.init

# Only run test in reset state as running CPU takes too much time to simulate
make \
  SIM=icarus \
  TOPLEVEL=ls180 \
  COCOTB_RESULTS_FILE=results_iverilog_ls180_wb.xml \
  COCOTB_HDL_TIMEUNIT=100ps \
  TESTCASE="wishbone_basic" \
  MODULE="testwb" \
  SIM_BUILD=sim_build_iverilog_wb_ls180



