#!/bin/sh

# Only run test in reset state as running CPU takes too much time to simulate
make \
  SIM=ghdl \
  COCOTB_RESULTS_FILE=results_iverilog.xml \
  COCOTB_HDL_TIMEUNIT=100ps \
  MODULE=test_add_boundary \
  TESTCASE="boundary_scan_reset" \
  SIM_BUILD=sim_build_ghdl


