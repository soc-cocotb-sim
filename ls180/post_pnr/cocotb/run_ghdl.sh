#!/bin/sh

# Only run test in reset state as running CPU takes too much time to simulate
make \
  SIM=ghdl \
  COCOTB_RESULTS_FILE=results_iverilog.xml \
  COCOTB_HDL_TIMEUNIT=100ps \
  TESTCASE="idcode_reset,idcodesvf_reset,idcode_run,idcode_run_svf" \
  SIM_BUILD=sim_build_iverilog


