#!/bin/sh

# create dummy memory files
yes 0 | head -128 > mem_1.init 
yes 0 | head -32 > mem_1.init 
touch mem.init mem_1.init mem_2.init mem_3.init mem_4.init

# Only run test in reset state as running CPU takes too much time to simulate
make \
  SIM=verilator \
  TOPLEVEL=chip \
  COCOTB_RESULTS_FILE=results_iverilator_chip.xml \
  COCOTB_HDL_TIMEUNIT=1ns \
  COCOTB_HDL_TIMEPRECISION=1ps \
  TESTCASE="idcode_reset,idcodesvf_reset,boundary_scan_reset,idcode_run" \
  VERILATOR_TRACE="1" \
  NOTUSEDCOMPILE_ARGS="--hierarchical " \
  NOTUSEDCOMPILE_ARGS="--unroll-count 256 \
        --output-split 5000 \
        --output-split-cfuncs 500 \
        --output-split-ctrace 500 \
        --prefix Vtop -o chip" \
  EXTRA_ARGS="--trace --trace-structs -Wno-fatal \
        -Wno-BLKANDNBLK \
        -Wno-TIMESCALEMOD \
        -Wno-WIDTH" \
  MODULE="test" \
  SIM_BUILD=sim_build_iverilator_chip



