# Dependencies

* make gitupdate
* https://git.libre-soc.org/?p=dev-env-setup.git;a=summary
  - run coriolis2_chroot (to set up coriolis2)
  - run install-hdl-apt-reqs
* install ghdl (working: GHDL 1.0-dev (v0.37.0-819-g9828b513) [Dunoon edition])
  this is very likely to have to be done from source
* install cocotb (can be done from source https://github.com/cocotb/cocotb)

# Build

* make one experiment with a JTAG tap (experiments10 or experiments9)
* copy *.vst into vst_src except cts files, chip.vst and chip_r.vst
* make cocotb
